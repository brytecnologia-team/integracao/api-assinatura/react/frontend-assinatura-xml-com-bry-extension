import React, { useEffect, useState } from "react";
import Select from "react-select";
import api from "../../services/api";
import { detectBrowser } from "../../utils/utils.js";

export default function Plugin() {

    // *************** Step 1: ***************

    const [document, setDocument] = useState(null);
    const [certificates, setCertificates] = useState("");
    const [selectedCertificate, setSelectedCertificate] = useState("");
    const [token, setToken] = useState("");
    const [signature, setSignature] = useState("");

    // Variable that calls function to check if the plugin is installed.
    const extensionInstalled = window.BryWebExtension.isExtensionInstalled();
    // Variable that calls function to check which browser is being used.
    const browser = detectBrowser();

    // *************** Step 2: ***************

    // If the extension is installed, it will list all certificates installed on the machine and add them to the certificates variable
    useEffect(() => {
        if (extensionInstalled) {
            // List certificates
            window.BryWebExtension.listCertificates().then(certificates => {
                certificates.forEach(certificate => {
                    certificate.label = certificate.name;
                });
                setCertificates(certificates);
            });
        }
    }, []);

    function handleSubmit(event) {
        // Prevents the page from updating when submitting

        event.preventDefault();

        sign();
    }
    async function sign() {
        // Data that must be sent in the request in JSON format

        const data = new FormData();
        data.append("certificate", selectedCertificate.certificateData);
        data.append("document", document);

        // ************** Step 3: ***************

        try {
            // Performs subscription initialization request to the backend application
            const response = await api.post("/initialize", data, {
                headers: { Authorization: token }
            });
            console.log(response.data);
            // Sends the content returned from initialization to be encrypted in the extension.
            extensionSign(response.data);
        } catch (err) {
            console.log(err.response.data.message);
            // Returns an alert if an error occurs during startup
            alert(err.response.data.message);
        }
    }

    // *************** Step 4: ***************

    async function extensionSign(response) {

        console.log(response)

        // Changes json values ​​returned from initialization to be sent in the format in which the extension accepts

        response.formatoDadosEntrada = "Base64";
        response.formatoDadosSaida = "Base64";
        response.algoritmoHash = "SHA256";
        response.assinaturas = response.signedAttributes;
        response.assinaturas[0].hashes = response.assinaturas[0].content;
        response.assinaturas.forEach(
            signature => (signature.hashes = [signature.hashes])
        );

        // Encrypts the initialized signature, passing the certificate ID and the response content of the signature initialization.

        await window.BryWebExtension.sign(
            selectedCertificate.certId,
            JSON.stringify(response)
        ).then(async signature => {
            console.log(signature);

            // Use the encrypted signature in the "signature" field to create the JSON that will be used to finalize the signature
            const data = new FormData();
            data.append("certificate", selectedCertificate.certificateData);
            data.append("signedNonce", signature.assinaturas[0].nonce);
            data.append("initializedDocuments", response.assinaturas[0].hashes);
            data.append("cifrado", signature.assinaturas[0].hashes);
            data.append("document", document);

            // *************** Step 5: ***************

            // Request for signature finalization

            try {
                console.log(document);
                const response = await api.post("/finalize", data, {
                    headers: { Authorization: token }
                });
                console.log(response);
                setSignature(JSON.stringify(response.data));
                // Download the signed file
                // window.location.href = "http://localhost:3333/baixarAssinatura";
            } catch (err) {
                alert(err.response.data.message);
            }
        });
    }
    return (
        <>
            {extensionInstalled ? (
                // Start of the rendered area if the extension is installed **
                <form onSubmit={handleSubmit}>
                    <h3>1º Passo: </h3>
                    <p>Selecione o certificado que deseja utilizar para assinar</p>
                    <Select
                        options={certificates}
                        onChange={event => setSelectedCertificate(event)}
                        value={selectedCertificate}
                        required
                    />
                    {selectedCertificate ? (
                        <React.Fragment>
                            <input
                                type="text"
                                readOnly
                                value={selectedCertificate.issuer || ""}
                            />
                            <input
                                type="text"
                                readOnly
                                value={selectedCertificate.expirationDate || ""}
                            />
                            <input
                                type="text"
                                readOnly
                                value={selectedCertificate.certificateType || ""}
                            />
                            <textarea
                                type="text"
                                rows="5"
                                value={selectedCertificate.certificateData}
                                readOnly
                            />
                        </React.Fragment>
                    ) : (
                        ""
                    )}
                    <h3>2º Passo: </h3>
                    <p>
                        Insira um Token de autenticação (Para conseguir um token válido
                        entre em contato com a equipe de suporte da BRy Tecnologia através
                        do email: helpdesk@bry.com.br
                    </p>
                    <input
                        id="token"
                        type="text"
                        required
                        onChange={event => setToken(event.target.value)}
                        placeholder="Token de autenticação BRy"
                    />{" "}
                    <h3>3º Passo: </h3>
                    <p>Selecione o documento a ser assinado *</p>
                    <label htmlFor="document" className="fileUp">
                        {document ? (
                            <React.Fragment>{document.name}</React.Fragment>
                        ) : (
                            <React.Fragment>
                                <i className="fa fa-upload"></i>
                                Selecione o arquivo
                            </React.Fragment>
                        )}
                        <input
                            id="document"
                            type="file"
                            required
                            onChange={event => setDocument(event.target.files[0])}
                        />
                    </label>
                    <button className="btn" type="submit">
                        Assinar
                    </button>
                    {signature ? (
                        <React.Fragment>
                            <h3>Assinatura: </h3>
                            <textarea type="text" rows="8" value={signature} readOnly />
                        </React.Fragment>
                    ) : (
                        ""
                    )}
                </form>
            ) : (
                // ** End of the rendered area if the extension is installed **

                // ** Start of area renders if extension is NOT installed **
                <div className="container">
                    {browser === "chrome" ? (
                        <div className="isChrome">
                            <h3 className="extension-message">
                                Detectamos que a Extensão para Assinatura Digital não está
                                instalada.
                            </h3>
                            <p>Segue abaixo um passo a passo para instalação da extensão</p>
                            <p>
                                <strong>1º Passo -</strong> Clique no botão abaixo para acessar
                                a Extensão no Chrome WebStore
                            </p>
                            <a
                                href="https://chrome.google.com/webstore/detail/mbpaklahifpfndjiefdfjhmkefppocfm"
                                className="btn btn-lg btn-primary btn-extension-install"
                            >
                                Instalar Extensão via Chrome WebStore!
                            </a>

                            <p>
                                <strong>2º Passo -</strong> Clique no botão{" "}
                                <strong>USAR NO CHROME</strong>
                            </p>
                            <img
                                alt="Print use on chrome button"
                                src={require("../../assets/imgs/use_on_chrome_button.jpg")}
                            />
                            <p>
                                <br />
                                <br />
                                <strong>3º Passo -</strong> Você deve retornar para esta página
                                que ela será atualizada.
                            </p>
                        </div>
                    ) : (
                        ""
                    )}
                    {browser === "firefox" ? (
                        <div className="isFirefox">
                            <h3 className="extension-message">
                                Detectamos que a extensão para Assinatura Digital não está
                                instalada.
                            </h3>

                            <p>Clique no botão abaixo para instalar a extensão</p>

                            <a
                                href="https://addons.mozilla.org/pt-BR/firefox/addon/assinatura-digital-navegador"
                                className="btn btn-lg btn-primary btn-extension-install"
                            >
                                Instalar Extensão!
                            </a>
                        </div>
                    ) : (
                        ""
                    )}
                    {browser === "edge" ? (
                        <div className="isEdge">
                            <h3 className="extension-message">
                                Lamentamos, mas uma versão da extensão só estará disponível para
                                o seu navegador na próxima atualização!
                            </h3>
                        </div>
                    ) : (
                        ""
                    )}
                    {browser === "opera" ? (
                        <div className="isOpera">
                            <h3 className="extension-message">
                                Lamentamos, mas uma versão da extensão só estará disponível para
                                o seu navegador na próxima atualização!
                            </h3>
                        </div>
                    ) : (
                        ""
                    )}

                    {browser === "safari" ? (
                        <div className="isSafari">
                            <h3>
                                Detectamos que a Extensão para Assinatura Digital não está
                                instalanda
                            </h3>
                            <input
                                type="image"
                                width="250"
                                src={require("../../assets/imgs/baixar.png")}
                                alt="Logo apple store"
                            />
                            <h4>
                                Após a instalação é necessário habilitar a extensão nas
                                preferências do Safari.
                            </h4>
                            <p>
                                <b>1º Abra o aplicativo "BRy Assinatura Digital" instalado</b>
                            </p>
                            <p>
                                <strong>
                                    2º Dentro do aplicativo, clique em habilitar a extensão nas
                                    preferências do Safari
                                </strong>
                            </p>
                            <img
                                src={require("../../assets/imgs/app.png")}
                                alt="Print button extension"
                            />
                            <p>
                                Caso a opção para habilitar a extensão não apareça nas
                                preferências do Safari, encerre o navagador, e repita o passo 2.
                            </p>
                            <p>
                                <strong>
                                    4º Após ativar a extensão, basta recarregar a página.
                                </strong>
                            </p>
                            <button onClick={document.location.reload(true)}>
                                Recarregar página
                            </button>
                        </div>
                    ) : (
                        ""
                    )}
                    {browser === "iE" ? (
                        <div className="isIE">
                            <div className="extension-message">
                                <h3>
                                    Lamentamos, mas uma versão da extensão só estará disponível
                                    para o seu navegador na próxima atualização!
                                </h3>
                            </div>
                        </div>
                    ) : (
                        ""
                    )}
                    {browser === "unknown" ? (
                        <div className="isUnknown">
                            <h3 className="extension-message">
                                Não foi possível identificar o seu browser!
                            </h3>
                        </div>
                    ) : (
                        ""
                    )}
                </div>
            )
                // ** End of area renders if extension is NOT installed **
            }
        </>
    );
}
