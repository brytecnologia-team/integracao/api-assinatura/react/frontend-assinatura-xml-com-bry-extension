import axios from 'axios';

// Create an axios instance with CORS options
const api = axios.create({
    baseURL: 'http://127.0.0.1:5000',
    withCredentials: true,
});

export default api;