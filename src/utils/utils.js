export function detectBrowser() {
    $("#chrome-browser").hide();
    $("#firefox-browser").hide();
    $("#opera-browser").hide();
    $("#edge-browser").hide();
    $("#safari-browser").hide();
    $("#ie-browser").hide();
    $("#unknown-browser").hide();
    if (isChrome()) {
        $("#chrome-browser").show();
    }
    else if (isFirefox()) {
        $("#firefox-browser").show();
    }
    else if (isEdge()) {
        $("#edge-browser").show();
    }
    else if (isOpera()) {
        $("#opera-browser").show();
    }
    else if (isSafari()) {
        $("#safari-browser").show();
    }
    else if (isIE()) {
        $("#ie-browser").hide();
    }
    else {
        $("#unknown-browser").hide();
    }
}
/**
 * Funções simplificadas para detecção do browser, modifique se achar necessário
 */
function isOpera() {
    return !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
}

function isFirefox() {
    return typeof InstallTrigger !== 'undefined';
}

function isSafari() {
    return navigator.userAgent.indexOf("Safari") > -1
}

function isIE() {
    return /*@cc_on!@*/ false || !!document.documentMode;
}

function isEdge() {
    if (document.documentMode || /Edge/.test(navigator.userAgent)) {
        return true;
    }
    else {
        return false;
    }
}

function isChrome() {
    return /Google Inc/.test(navigator.vendor);
}



export function isExtensionInstalled() {
    window.BryWebExtension.isExtensionInstalled()
        .then((installed) => {
            if (installed) {
                /*
                 Verifica se o módulo nativo está instalado
                 e se necessário solicita
                 que o usuário realize a instalação
                */
                return window.BryWebExtension.installComponents();

            } else {
                // Extensão não instalada
            }
        })
        .then((installComponentsResponse) => {
            // Componentes instalados com sucesso!
            window.BryApiModule.listCertificates();
        })
        .catch((error) => {
            $("#codigo-de-erro").html(error.key);
            $("#error-message-text").text(error.description);
            $("#error-message").show();
            $("#success-message").hide();
            $("#update").hide();
        })
};

export function validaCPF(strCPF) {
    var Soma;
    var Resto;
    Soma = 0;
    if (strCPF === "00000000000") return false;

    for (let i = 1; i <= 9; i++)
        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    Resto = (Soma * 10) % 11;

    if (Resto === 10 || Resto === 11) Resto = 0;
    if (Resto !== parseInt(strCPF.substring(9, 10))) return false;

    Soma = 0;
    for (let i = 1; i <= 10; i++)
        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;

    if (Resto === 10 || Resto === 11) Resto = 0;
    if (Resto !== parseInt(strCPF.substring(10, 11))) return false;
    return true;
}
