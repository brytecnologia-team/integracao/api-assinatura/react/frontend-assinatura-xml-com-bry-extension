# Frontend ReactJS de Geração de Assinatura XML

Este é um exemplo de integração em ReactJS que utiliza a extensão para cifrar os dados, integrado com o backend em Python.

Este exemplo apresenta os passos necessários para a geração de assinatura XML utilizando certificado digital instalado no repositório pessoal, podendo ser o repositório do Windows, do Mac ou do Mozilla Firefox (nas distribuições linux).
  - Passo 1: Recebe o documento, o AuthToken e os dados de um certificado instalado (Necessita ter a BRy Extension instalada).
  - Passo 2: Lista os certificados instalados.
  - Passo 3: Envia a requisição de inicialização para o backend/python.
  - Passo 4: Cifragem dos dados inicializados com o certificado selecionado.
  - Passo 5: Finalização da assinatura e obtenção do artefato assinado.

### Backend

Este exemplo frontend é compatível com os backends:

* [Python](https://gitlab.com/brytecnologia-team/integracao/api-assinatura/python/assinatura-xml-com-bry-extension)

### Tech

O exemplo utiliza das bibliotecas JavaScript abaixo:
* [ReactJS] - A JavaScript library for building user interfaces!
* [Axios] - Promise based HTTP client for the browser and node.js.
* [Express] - With a myriad of HTTP utility methods and middleware at your disposal, creating a robust API is quick and easy.

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

Além disso, no processo de geração de assinatura é obrigatório a posse de um certificado digital que identifica o autor do artefato assinado que será produzido.

Por esse motivo, é necessário configurar a BRy Extension no seu browser, assim como ter um certificado importado.

**Observação**

É necessário ter o [Node] instalado na sua máquina para a instalação das dependências e execução do projeto, além de executar esta aplicação juntamente com o backend (Python).

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a produção da assinatura.

| Variável  |                   Descrição                   | 
| --------- | --------------------------------------------- |
| AuthToken | Access Token para o consumo do serviço (JWT). |


## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Uso

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência e instale as dependências. 
Utilizamos o ReactJS versão 16.10 para desenvolvimento e o [Npm] versão 6.13 ou o [Yarn] versão 1.22 para instalação das dependências e execução da aplicação.

##### Comandos:

**Instalar as dependências utilizando o comando abaixo:**

    - npm install

  ou

    - yarn

**Executar programa:**

    - npm start

  ou

    - yarn start

   [Node]: <https://nodejs.org/en/>
   [ReactJS]: <https://reactjs.org/>
   [Axios]: <https://github.com/axios/axios>
   [Express]: <https://expressjs.com/cycastle.org/>
   [Yarn]: <https://yarnpkg.com/>
   [Npm]: <https://www.npmjs.com/>
